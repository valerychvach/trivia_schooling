﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using GameSparks.Api.Responses;

public class GameSparksUserManager : MonoBehaviour
{
    [FoldoutGroup("UI Elements")]
    public InputField text_username, text_pass, text_usernameC, text_passC, text_passConfirmC;
    public StringVariableSO loadingMessage;

    [FoldoutGroup("Events")]
    public GameEvent Event_User_OnAuthenticationRequest;
    [FoldoutGroup("Events")]
    public GameEvent Event_User_OnAuthenticationSuccess;
    [FoldoutGroup("Events")]
    public GameEvent Event_User_OnAuthenticationFailure;
    [FoldoutGroup("Events")]
    public GameEvent Event_User_OnRegistrationRequest;
    [FoldoutGroup("Events")]
    public GameEvent Event_User_OnRegistrationSuccess;
    [FoldoutGroup("Events")]
    public GameEvent Event_User_OnRegistrationFailure;

    public void LogIn()
    {
        GameSparksManager.Instance().AuthenticateUser(text_username.text, text_pass.text, OnAuthentication);
        loadingMessage.value = "LOGGING IN";
        Event_User_OnAuthenticationRequest.Raise();
    }

    public void Register()
    {
        Debug.Log("GSM| Registering player");
        if (text_passC.text != text_passConfirmC.text)
        {
            Debug.Log("GSM| Passwords don't match");
            Event_User_OnRegistrationFailure.Raise("Passwords don't match");
            return;
        }
        GameSparksManager.Instance().RegistrateUser(text_usernameC.text, text_passC.text, OnRegistration);
        loadingMessage.value = "CREATING NEW ACCOUNT";
        Event_User_OnRegistrationRequest.Raise();
    }

    /// <summary>
    /// This is called when a player is authenticated
    /// </summary>
    /// <param name="_resp">Resp.</param>
    private void OnAuthentication(AuthenticationResponse _resp)
    {
        if (!_resp.HasErrors)
        {
            Debug.Log("GSM| Authentication Successful...");
            Event_User_OnAuthenticationSuccess.Raise(_resp.DisplayName);
        }
        else
        {
            Debug.LogWarning("GSM| Error Authenticating User \n" + _resp.Errors.JSON);
            Event_User_OnAuthenticationFailure.Raise();
        }
    }

    /// <summary>
    /// this is called when a player is registered
    /// </summary>
    /// <param name="_resp">Resp.</param>
    private void OnRegistration(RegistrationResponse _resp)
    {

        if (!_resp.HasErrors)
        { // if we get the response back with no errors then the registration was successful
            Debug.Log("GSM| Registration Successful...");
            Event_User_OnRegistrationSuccess.Raise();
        }
        else
        {
            Debug.Log("GSM| Registration error");
            Debug.Log(_resp.Errors);
            Event_User_OnRegistrationFailure.Raise("Username already taken");
        }
    }
}
