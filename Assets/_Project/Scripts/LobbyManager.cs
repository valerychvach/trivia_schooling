﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using TMPro;
using GameSparks.RT;
using UnityEngine.SceneManagement;

public class LobbyManager : MonoBehaviour
{
    [FoldoutGroup("Match making")]
    public Toggle toggle_quee2;
    [FoldoutGroup("Match making")]
    public Toggle toggle_quee4;
    [FoldoutGroup("Lobby")]
    public TextMeshProUGUI status;
    [FoldoutGroup("Lobby")]
    public List<LobbyPlayerInfo> playersInfo;

    [FoldoutGroup("Events")]
    public GameEvent Event_Lobby_OnPlayersReady;
    [FoldoutGroup("Events")]
    public GameEvent Event_Game_LoadScene;

    void Start()
    {
        GameSparks.Api.Messages.MatchFoundMessage.Listener += OnMatchFound;
        GameSparks.Api.Messages.MatchUpdatedMessage.Listener += OnMatchUpdated;
        new GameSparks.Api.Requests.AccountDetailsRequest().Send((response) =>
        {
            playersInfo[0].SetPlayer(response.DisplayName, response.UserId);
        });
    }

    public void FindMatch()
    {
        if (toggle_quee2.isOn)
        {
            GameSparksManager.Instance().FindPlayers(2);
            playersInfo[1].holder.SetActive(false);
            playersInfo[3].holder.SetActive(false);
            status.text = "Waiting for all players (1/2)";
        }
        else
        {
            // if (toggle_quee4.isOn)
            GameSparksManager.Instance().FindPlayers(4);
            playersInfo[1].holder.SetActive(true);
            playersInfo[3].holder.SetActive(true);
            status.text = "Waiting for all players (1/4)";
        }
    }

    private void OnMatchFound(GameSparks.Api.Messages.MatchFoundMessage _message)
    {
        Debug.Log("Match Found!...");
        if (_message.MatchShortCode == "quee2")
        {
            foreach (GameSparks.Api.Messages.MatchFoundMessage._Participant player in _message.Participants)
            {
                if (player.Id != playersInfo[0].playerId)
                {
                    playersInfo[2].SetPlayer(player.DisplayName, player.Id);
                }
                for (int i = 0; i < playersInfo.Count; i++)
                {
                    if (playersInfo[i].playerId == player.Id)
                        playersInfo[i].peerId = (int)player.PeerId;
                }
            }
            status.text = "Waiting for all players (2/2)";
        }
        else
        {
            foreach (GameSparks.Api.Messages.MatchFoundMessage._Participant player in _message.Participants)
            {
                for (int i = 0; i < playersInfo.Count; i++)
                {
                    if (playersInfo[i].playerId == player.Id)
                        playersInfo[i].peerId = (int)player.PeerId;
                }
            }
            status.text = "Waiting for all players (4/4)";
        }
        GameSparksManager.Instance().StartNewRTSession(new GameSparksManager.RTSessionInfo(_message));
    }

    private void OnMatchUpdated(GameSparks.Api.Messages.MatchUpdatedMessage _message)
    {
        Debug.Log("Match updated...");
        if (_message.MatchShortCode == "quee4") //If its a 1v1 quee
        {
            int nParticipants = 1;
            // Debug.Log(playersInfo[0].playerId);
            foreach (GameSparks.Api.Messages.MatchUpdatedMessage._Participant player in _message.Participants)
            {
                // Debug.Log(player.Id);
                if (player.Id != playersInfo[0].playerId)
                {
                    playersInfo[nParticipants].SetPlayer(player.DisplayName, player.Id);
                    nParticipants++;
                }
            }
            status.text = "Waiting for all players (" + nParticipants + "/4)";
            // Debug.Log(playersInfo[0].playerId);
            for (int i = nParticipants; i < playersInfo.Count; i++)
            {
                playersInfo[i].RemovePlayer();
            }
        }
        // Debug.Log(_message.JSONString);
    }

    [System.Serializable]
    public class LobbyPlayerInfo
    {
        public GameObject holder;
        public TextMeshProUGUI text_displayName;
        public GameObject image_profile;
        public GameObject image_disconnected;
        public int peerId;
        public string playerId;
        public bool taken;
        [Space(10)]
        public bool isReady;
        public GameObject ready;
        public GameObject unready;

        public void SetPlayer(string displayName, string id)
        {
            text_displayName.text = displayName;
            playerId = id;
            image_profile.SetActive(true);
            image_disconnected.SetActive(false);
            taken = true;
        }

        public void SetPlayer(LobbyPlayerInfo other)
        {
            text_displayName.text = other.text_displayName.text;
            playerId = other.playerId;
            image_profile.SetActive(true);
            image_disconnected.SetActive(false);
            taken = true;
        }

        public void RemovePlayer()
        {
            text_displayName.text = "Disconnected";
            playerId = "";
            image_profile.SetActive(false);
            image_disconnected.SetActive(true);
            taken = false;
        }

        public void ToggleReady()
        {
            isReady = !isReady;
            ready.SetActive(isReady);
            unready.SetActive(!isReady);
        }
    }

    public void ToggleReady()   //Local player toggles ready state
    {
        playersInfo[0].ToggleReady();
        RTData data = new RTData();
        data.SetInt(1, 0);
        GameSparksManager.Instance().GetRTSession().SendData(1, GameSparksRT.DeliveryIntent.RELIABLE, data);
        EvalutePlayersReady();
    }

    public void Receiving_ToggleReady(int peerId) //Player in lobby toggles ready state
    {
        Debug.Log("Player " + peerId + " toggled ready");
        for (int i = 1; i < playersInfo.Count; i++)
        {
            if (playersInfo[i].peerId == peerId)
            {
                playersInfo[i].ToggleReady();
                break;
            }
        }
        EvalutePlayersReady();
    }

    void EvalutePlayersReady()
    {
        for (int i = 0; i < playersInfo.Count; i++)
        {
            if (playersInfo[i].taken)
            {
                if (!playersInfo[i].isReady)
                    return;
            }
        }
        Event_Lobby_OnPlayersReady.Raise();
        Event_Game_LoadScene.Raise(2);
    }
}
