﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;

[CanEditMultipleObjects]
[CustomEditor(typeof(GameEvent))]
public class EventEditor : OdinEditor
{
    bool showParameters;
    string parameters;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GameEvent e = target as GameEvent;
        GUI.backgroundColor = e._colorIdentifier;

        GUI.enabled = Application.isPlaying;

        if (GUILayout.Button("Raise"))
        {
            if (parameters != null && parameters != "")
                e.Raise(parameters);
            else
                e.Raise();
        }

        EditorGUILayout.Foldout(showParameters, "Does event need parameters?");
        if (showParameters)
        {
            EditorGUILayout.TextField("Parameters: ", parameters);
        }

        GUI.enabled = true;
        GUILayout.Space(20);
        if (GUILayout.Button("Update listeners"))
        {
            e.listeners.Clear();
            var gos = Resources.FindObjectsOfTypeAll<GameEventListener>();
            if (gos.Length > 0)
            {
                string mssg = "";
                for (int i = 0; i < gos.Length; i++)
                {
                    for (int j = 0; j < gos[i].Event.Count; j++)
                    {
                        if (gos[i].Event[j] == e)
                        {
                            e.listeners.Add(gos[i].gameObject);
                            mssg += "\n" + gos[i].gameObject.name;
                            EditorGUIUtility.PingObject(gos[i].gameObject);
                            break;
                        }
                    }
                }
                Debug.Log(e.listeners.Count + " listeners found.\n" + mssg);
            }
            else
            {
                Debug.LogWarning("No listeners found");
            }
        }
    }
}
