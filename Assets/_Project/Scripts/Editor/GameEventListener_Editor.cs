﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector.Editor;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameEventListener)), CanEditMultipleObjects]
public class GameEventListener_Editor : OdinEditor
{
    public override void OnInspectorGUI()
    {
        GameEventListener e = target as GameEventListener;
        if (e.Event != null && e.Event.Count == 1)
        {
            GUI.backgroundColor = e.Event[0]._colorIdentifier;
        }
        base.OnInspectorGUI();
    }
}
