﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IntVariable_", menuName = "Trivia/Int variable")]
public class IntVariableSO : ScriptableObject
{
    public int value;
    [TextArea]
    public string description;
}
