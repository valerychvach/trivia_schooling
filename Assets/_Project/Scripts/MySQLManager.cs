﻿using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class MySQLManager : MonoBehaviour
{
    [InlineEditor]
    public QuestionsSO questionsSO;

    public GameEvent Event_OnAwake;
    public GameEvent Event_OnDownloadComplete;
    public GameEvent Event_OnUploadStarted;
    public GameEvent Event_OnUploadComplete;

    void Awake()
    {
        Event_OnAwake.Raise();
        GetData();
    }

    [Button("Send data")]
    public void SendData()
    {
        StartCoroutine(SendDataCo());
    }
    IEnumerator SendDataCo()
    {
        Event_OnUploadStarted.Raise("Uploading data to server");
        WWWForm form = new WWWForm();
        form.AddField("JSON", JsonUtility.ToJson(questionsSO.questionsData));

        UnityWebRequest www = UnityWebRequest.Post("http://josepalacio.info/TriviaGame/UpdateData.php", form);
        www.chunkedTransfer = false;
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Event_OnUploadComplete.Raise("Error uploading data");
            Debug.Log(www.error);
        }
        else
        {
            Event_OnUploadComplete.Raise("Data uploaded!");
            Debug.Log("Form upload complete!");
        }
    }

    [Button("Get data")]
    public void GetData()
    {
        Debug.Log("Getting server data");
        StartCoroutine(GetDataCo());
    }
    IEnumerator GetDataCo()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://josepalacio.info/TriviaGame/UpdateData.php");
        // www.chunkedTransfer = false;
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Download complete!");
            questionsSO.questionsData = JsonUtility.FromJson<QuestionsData>(www.downloadHandler.text);
            Event_OnDownloadComplete.Raise("Data downloaded!");
        }
    }
}
