﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "QuestionsData", menuName = "Trivia/QuestionsData")]
public class QuestionsSO : ScriptableObject
{
    public QuestionsData questionsData;
}

[System.Serializable]
public class QuestionsData
{
    [ListDrawerSettings(NumberOfItemsPerPage = 4)]
    public List<Category> categories;
    [ListDrawerSettings(NumberOfItemsPerPage = 3)]
    public List<Questions> questions;

}

[System.Serializable]
public class Category
{
    [HorizontalGroup("Category", 0.5f)]
    public string name;
    [HorizontalGroup("Category", 0.5f)]
    public int spriteId;
    [HorizontalGroup("Levels", 0.5f)]
    public List<string> levelsOfKnowledge;
    [HorizontalGroup("Levels", 0.5f)]
    public List<int> levelsOfKnowledgeXP;

    public Category(string _name, int _spriteId)
    {
        name = _name;
        spriteId = _spriteId;
    }
}

[System.Serializable]
public class Questions
{
    public string category;
    public string levelOfKnowledge;
    public string question;

    [HorizontalGroup("Answers", 0.5f)]
    public string[] answers;
    [HorizontalGroup("Answers", 0.5f), LabelText("Which are the correct ones")]
    public int[] answers_isCorrect;

    public Questions()
    {
        answers = new string[4];
        answers_isCorrect = new int[4];
    }
}
