﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Helper_StringDisplayer : MonoBehaviour
{
    TextMeshProUGUI textM;
    public StringVariableSO target;
    void Awake()
    {
        textM = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        textM.text = target.value;
    }

}
