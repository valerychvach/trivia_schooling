﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CAnimationSO", menuName = "Custom/CAnimationSO")]
public class CAnimationSO : ScriptableObject
{
    public List<AnimationCurve> customCurves;
}
