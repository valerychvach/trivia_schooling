﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class CAnimation : MonoBehaviour
{
    [InlineEditor]
    public CAnimationSO cAnimationSO;
    [FoldoutGroup("Testing"), ValueDropdown("AnimationsTags"), InlineButton("TestPlay"), InlineButton("TestStop")]
    public string testingTag_Play;
    public List<CAnimation_Data> animations;

    private List<string> AnimationsTags()
    {
        List<string> temp = new List<string>();
        for (int i = 0; i < animations.Count; i++)
        {
            if (!temp.Contains(animations[i].tag)) temp.Add(animations[i].tag);
        }
        return temp;
    }
    RectTransform rectTransform;
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    void TestPlay()
    {
        StartAnimation(testingTag_Play);
    }

    void TestStop()
    {
        StopAnimation(testingTag_Play);
    }
    public void StartAnimation(string _tag)
    {
        for (int i = 0; i < animations.Count; i++)
        {
            if (animations[i].tag.ToLower() == _tag.ToLower())
            {
                switch (animations[i].animationType)
                {
                    case CAnimation_Data.CAnimationType.RectPosition:
                        CAnimation_RectTransform_Position crp = gameObject.AddComponent<CAnimation_RectTransform_Position>();
                        crp.data = animations[i];
                        crp.rectTransform = rectTransform;
                        break;
                    case CAnimation_Data.CAnimationType.RectScale:
                        CAnimation_RectTransform_Scale crs = gameObject.AddComponent<CAnimation_RectTransform_Scale>();
                        crs.data = animations[i];
                        crs.rectTransform = rectTransform;
                        break;
                    case CAnimation_Data.CAnimationType.RectRotation:
                        CAnimation_RectTransform_Rotation crr = gameObject.AddComponent<CAnimation_RectTransform_Rotation>();
                        crr.data = animations[i];
                        crr.rectTransform = rectTransform;
                        break;
                    case CAnimation_Data.CAnimationType.Scale:
                        CAnimation_Scale cs = gameObject.AddComponent<CAnimation_Scale>();
                        cs.data = animations[i];
                        break;
                    case CAnimation_Data.CAnimationType.Alpha:
                        CAnimation_Alpha ca = gameObject.AddComponent<CAnimation_Alpha>();
                        ca.data = animations[i];
                        ca.image = GetComponent<Image>();
                        break;
                }
            }
        }
    }

    void StopAnimation(string _tag)
    {
        CAnimationBase[] currentAnimations = GetComponents<CAnimationBase>();
        for (int i = 0; i < currentAnimations.Length; i++)
        {
            if (currentAnimations[i].data.tag.ToLower() == _tag.ToLower())
            {
                Destroy(currentAnimations[i]);
            }
        }
    }
}

[System.Serializable]
public class CAnimation_Data
{
    public enum CAnimationType
    {
        RectPosition,
        RectScale,
        RectRotation,
        Scale,
        Alpha
    }
    public CAnimationType animationType;
    public string tag;
    public bool overrideStartingPoint;
    [ShowIf("overrideStartingPoint"), HorizontalGroup("From", 0.3f), LabelText("x:"), LabelWidth(20)]
    public float from_x;
    [ShowIf("overrideStartingPoint"), HorizontalGroup("From", 0.3f), LabelText("y:"), LabelWidth(20), HideIf("hideY")]
    public float from_y;
    [ShowIf("overrideStartingPoint"), HorizontalGroup("From", 0.3f), LabelText("z:"), LabelWidth(20), HideIf("hideZ")]
    public float from_z;
    [HorizontalGroup("To", 0.3f), LabelText("x:"), LabelWidth(20)]
    public float to_x;
    [HorizontalGroup("To", 0.3f), LabelText("y:"), LabelWidth(20), HideIf("hideY")]
    public float to_y;
    [HorizontalGroup("To", 0.3f), LabelText("z:"), LabelWidth(20), HideIf("hideZ")]
    public float to_z;
    public AnimationCurve easing;
    public float duration;
    public bool loop;
    // [DrawWithUnity, FoldoutGroup("Events")]
    // public UnityEvent OnStart;
    [DrawWithUnity, FoldoutGroup("Events")]
    public UnityEvent OnStart;
    [DrawWithUnity, FoldoutGroup("Events")]
    public UnityEvent OnComplete;

    #region Privates

    bool hideY()
    {
        return
        animationType == CAnimationType.RectRotation ||
        animationType == CAnimationType.Alpha;
    }
    bool hideZ()
    {
        return
        animationType == CAnimationType.RectScale ||
        animationType == CAnimationType.RectRotation ||
        animationType == CAnimationType.Alpha;
    }
    #endregion
}
