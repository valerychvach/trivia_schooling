﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAnimationBase : MonoBehaviour
{
    public CAnimation_Data data;
    public float timeStarted;

    void Start()
    {
        timeStarted = Time.time;
        Initialize();
    }

    void Update()
    {
        if (Time.time >= timeStarted + data.duration)
        {
            data.OnComplete.Invoke();
            if (!data.loop)
            {
                Destroy(this);
            }
            else
            {
                timeStarted = Time.time;
                data.OnStart.Invoke();
            }
        }
        Tween();
    }

    public virtual void Initialize() { }

    public virtual void Tween() { }

    public void Stop(string _tag)
    {
        if (data.tag.ToLower() == _tag.ToLower())
            Destroy(this);
    }
}
