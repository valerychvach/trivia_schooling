﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public class CAnimation_RectTransform_Rotation : CAnimationBase
{
    public RectTransform rectTransform;

    float from;
    float to;
    float amount;
    // Use this for initialization
    public override void Initialize()
    {
        from = data.from_x;
        if (data.overrideStartingPoint)
            rectTransform.eulerAngles = new Vector3(rectTransform.eulerAngles.x, rectTransform.eulerAngles.y, from);
        else
            from = rectTransform.eulerAngles.z;
        to = data.to_x;
        amount = to - from;

        data.OnStart.Invoke();
    }

    // Update is called once per frame
    public override void Tween()
    {
        rectTransform.eulerAngles = new Vector3(rectTransform.eulerAngles.x, rectTransform.eulerAngles.y, from + (amount * data.easing.Evaluate((Time.time - timeStarted) / data.duration)));
    }
}
