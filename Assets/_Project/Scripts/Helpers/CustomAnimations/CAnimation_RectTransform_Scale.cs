﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public class CAnimation_RectTransform_Scale : CAnimationBase
{
    public RectTransform rectTransform;

    Vector2 from;
    Vector2 to;
    Vector2 amount;
    // Use this for initialization
    public override void Initialize()
    {
        from = new Vector2(data.from_x, data.from_y);
        if (data.overrideStartingPoint)
            rectTransform.sizeDelta = from;
        else
            from = rectTransform.sizeDelta;
        to = new Vector2(data.to_x, data.to_y);
        amount = to - from;

        data.OnStart.Invoke();
    }

    // Update is called once per frame
    public override void Tween()
    {
        rectTransform.sizeDelta = from + (amount * data.easing.Evaluate((Time.time - timeStarted) / data.duration));
    }
}
