﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public class CAnimation_RectTransform_Position : CAnimationBase
{
    public RectTransform rectTransform;
    Vector3 from;
    Vector3 to;
    Vector3 amount;
    // Use this for initialization
    public override void Initialize()
    {
        from = new Vector3(data.from_x, data.from_y, data.from_z);
        if (data.overrideStartingPoint)
            rectTransform.anchoredPosition3D = from;
        else
            from = rectTransform.anchoredPosition3D;
        to = new Vector3(data.to_x, data.to_y, data.to_z);
        amount = to - from;

        data.OnStart.Invoke();
    }

    // Update is called once per frame
    public override void Tween()
    {
        rectTransform.anchoredPosition3D = from + (amount * data.easing.Evaluate((Time.time - timeStarted) / data.duration));
    }
}
