﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CAnimation_Alpha : CAnimationBase
{
    public Image image;

    float from;
    float to;
    float amount;
    // Use this for initialization
    public override void Initialize()
    {
        from = data.from_x;
        if (data.overrideStartingPoint)
            image.color = new Color(image.color.r, image.color.g, image.color.b, from);
        else
            from = image.color.a;
        to = data.to_x;
        amount = to - from;

        data.OnStart.Invoke();
    }

    // Update is called once per frame
    public override void Tween()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, from + (amount * data.easing.Evaluate((Time.time - timeStarted) / data.duration)));
    }
}
