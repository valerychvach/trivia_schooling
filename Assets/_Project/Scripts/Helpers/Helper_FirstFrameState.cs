﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class Helper_FirstFrameState : MonoBehaviour
{
    [DrawWithUnity]
    public UnityEvent OnFirstFrame;
    void Awake()
    {
        OnFirstFrame.Invoke();
    }
}
