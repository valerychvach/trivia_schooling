﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComparerFloat : IComparer<float>
{
    public int Compare(float a, float b)
    {
        if (a < b)
            return -1;
        if (a == b)
            return 0;
        return 1;
    }
}
