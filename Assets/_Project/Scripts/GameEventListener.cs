﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using Sirenix.OdinInspector;


public class GameEventListener : MonoBehaviour
{
    [Tooltip("Event to register with."), InlineEditor]
    public List<GameEvent> Event;

    [FoldoutGroup("Booleans")]
    public bool nr, sr, tr, ir, br;

    [Tooltip("Response to invoke when Event is raised."), ShowIf("nr")]
    [FoldoutGroup("UnityEvent"), DrawWithUnity]
    public UnityEvent Response;

    [Tooltip("Response with parameters to invoke when Event is raised, only if parameters are needed otherwise leave it empty")]
    [FoldoutGroup("UnityEvent"), DrawWithUnity, ShowIf("sr")]
    public UnityEvent_string Response_withParameters;

    [FoldoutGroup("UnityEvent"), DrawWithUnity, ShowIf("tr")]
    public UnityEvent_Transform Response_withTransfrom;

    [FoldoutGroup("UnityEvent"), DrawWithUnity, ShowIf("ir")]
    public UnityEvent_int Response_withInt;

    [FoldoutGroup("UnityEvent"), DrawWithUnity, ShowIf("br")]
    public UnityEvent_bool Response_withBool;

    private void OnEnable()
    {
        for (int i = 0; i < Event.Count; i++)
        {
            Event[i].RegisterListener(this);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < Event.Count; i++)
        {
            Event[i].UnregisterListener(this);
        }
    }

    public void OnEventRaised()
    {
        Response.Invoke();
    }

    public void OnEventRaised_Parameters(string parameters)
    {
        Response_withParameters.Invoke(parameters);
    }

    public void OnEventRaised_Transform(Transform _t)
    {
        Response_withTransfrom.Invoke(_t);
    }

    public void OnEventRaised_Int(int i)
    {
        Response_withInt.Invoke(i);
    }

    public void OnEventRaised_Bool(bool flag)
    {
        Response_withBool.Invoke(flag);
    }
}


[System.Serializable]
public class UnityEvent_string : UnityEvent<string>
{
}

[System.Serializable]
public class UnityEvent_Transform : UnityEvent<Transform>
{
}

[System.Serializable]
public class UnityEvent_int : UnityEvent<int>
{
}

[System.Serializable]
public class UnityEvent_bool : UnityEvent<bool>
{
}