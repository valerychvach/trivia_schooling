﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BoolVariable_", menuName = "Trivia/Bool variable")]
public class BoolVariableSO : ScriptableObject
{
    public bool value;
    [TextArea]
    public string description;

    public void SetValue(bool val)
    {
        value = val;
    }
}
