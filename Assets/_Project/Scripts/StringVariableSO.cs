﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StringVariable_", menuName = "Trivia/String variable")]
public class StringVariableSO : ScriptableObject
{

    public string value;
    [TextArea]
    public string description;

    public void SetValue(string val)
    {
        value = val;
    }
}
