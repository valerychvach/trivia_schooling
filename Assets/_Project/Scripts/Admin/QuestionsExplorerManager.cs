﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using TMPro;

public class QuestionsExplorerManager : MonoBehaviour
{
    [InlineEditor]
    public QuestionsSO questionSO;
    [ReadOnly]
    public Sprite[] icons;

    [FoldoutGroup("Category explorer")]
    public Transform content_categoryExplorer;
    [FoldoutGroup("Category explorer")]
    public GameObject questionCategoryExplorerItem;

    [FoldoutGroup("Questions viewer")]
    public TextMeshProUGUI viewer_categoryName;
    [FoldoutGroup("Questions viewer")]
    public Image viewer_categoryIcon;
    [FoldoutGroup("Questions viewer")]
    public Transform content_questionsViewer;
    [FoldoutGroup("Questions viewer")]
    public GameObject questionItem;

    [FoldoutGroup("Questions editor")]
    public TextMeshProUGUI editor_categoryName;
    [FoldoutGroup("Questions editor")]
    public Image editor_categoryIcon;
    [FoldoutGroup("Questions editor")]
    public Dropdown levelDropDown;
    [FoldoutGroup("Questions editor")]
    public InputField questionInput;
    [FoldoutGroup("Questions editor"), HorizontalGroup("answers", 0.5f)]
    public Toggle[] answersCorrect;
    [FoldoutGroup("Questions editor"), HorizontalGroup("answers", 0.5f)]
    public InputField[] answers;

    int editingCategoryId;
    int editingQuestionId;
    void Awake()
    {
        icons = Resources.LoadAll<Sprite>("CategoryIcons");
    }

    #region Category explorer
    public void SetCategoryExplorer()
    {
        if (content_categoryExplorer.childCount > 0)
            //Erase all children
            for (int i = content_categoryExplorer.childCount - 1; i >= 0; i--)
            {
                Destroy(content_categoryExplorer.GetChild(i).gameObject);
            }

        //Fill content with all saved categories
        for (int i = 0; i < questionSO.questionsData.categories.Count; i++)
        {
            QuestionsExplorerCategoryItem c = ((GameObject)Instantiate(questionCategoryExplorerItem, content_categoryExplorer)).GetComponent<QuestionsExplorerCategoryItem>();
            c.SetItem(i, questionSO.questionsData.categories[i].name, icons[questionSO.questionsData.categories[i].spriteId]);
        }
        content_categoryExplorer.GetComponent<RectTransform>().sizeDelta = new Vector2(
            content_categoryExplorer.GetComponent<RectTransform>().sizeDelta.x,
            questionCategoryExplorerItem.GetComponent<RectTransform>().sizeDelta.y * questionSO.questionsData.categories.Count);
    }
    #endregion

    #region Questions viewer
    public void SetQuestionsViewer(int categoryId)
    {
        editingCategoryId = categoryId;
        viewer_categoryName.text = questionSO.questionsData.categories[categoryId].name;
        viewer_categoryIcon.sprite = icons[questionSO.questionsData.categories[categoryId].spriteId];

        if (content_questionsViewer.childCount > 0)
            //Erase all children
            for (int i = content_questionsViewer.childCount - 1; i >= 0; i--)
            {
                Destroy(content_questionsViewer.GetChild(i).gameObject);
            }

        int nQuestions = 0;
        //Fill content with all questions in the current category ordered by Level of Knowledge
        for (int i = 0; i < questionSO.questionsData.categories[categoryId].levelsOfKnowledge.Count; i++)
        {
            for (int j = 0; j < questionSO.questionsData.questions.Count; j++)
            {
                if (questionSO.questionsData.questions[j].category == questionSO.questionsData.categories[categoryId].name
                && questionSO.questionsData.questions[j].levelOfKnowledge == questionSO.questionsData.categories[categoryId].levelsOfKnowledge[i])
                {	//If the question belongs to the same category and the same level of knowledge
                    QuestionExplorerItem c = ((GameObject)Instantiate(questionItem, content_questionsViewer)).GetComponent<QuestionExplorerItem>();
                    c.Set(j, questionSO.questionsData.questions[j].question, questionSO.questionsData.questions[j].levelOfKnowledge);
                    nQuestions++;
                }
            }
        }
        content_questionsViewer.GetComponent<RectTransform>().sizeDelta = new Vector2(
            content_questionsViewer.GetComponent<RectTransform>().sizeDelta.x,
            questionItem.GetComponent<RectTransform>().sizeDelta.y * nQuestions);
    }

    public void RemoveQuestion(int id)
    {
        questionSO.questionsData.questions.RemoveAt(id);
        SetQuestionsViewer(editingCategoryId);
    }
    #endregion

    #region Questions editor
    public void SetQuestionsEditor(int id)
    {
        editingQuestionId = id;
        if (id >= 0)
        {
            editor_categoryName.text = questionSO.questionsData.categories[editingCategoryId].name;
            editor_categoryIcon.sprite = icons[questionSO.questionsData.categories[editingCategoryId].spriteId];

            //Set levels dropdown values
            levelDropDown.ClearOptions();
            levelDropDown.AddOptions(questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge);
            for (int i = 0; i < questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge.Count; i++)
            {
                if (questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge[i] == questionSO.questionsData.questions[id].levelOfKnowledge)
                {
                    levelDropDown.value = i;
                    break;
                }
            }

            //Set question value
            questionInput.text = questionSO.questionsData.questions[id].question;

            //Set answers value
            for (int i = 0; i < 4; i++)
            {
                answersCorrect[i].isOn = questionSO.questionsData.questions[id].answers_isCorrect[i] == 1;
                answers[i].text = questionSO.questionsData.questions[id].answers[i];
            }
        }
        else
        {	//Is question being added
            Questions q = new Questions();
            q.category = questionSO.questionsData.categories[editingCategoryId].name;
            q.levelOfKnowledge = questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge[0];
            q.question = "Some question";
            q.answers = new string[]{
                "Some answer",
                "Some answer",
                "Some answer",
                "Some answer"
            };
            q.answers_isCorrect = new int[] { 1, 0, 0, 0 };
            questionSO.questionsData.questions.Add(q);
            SetQuestionsEditor(questionSO.questionsData.questions.Count - 1);
        }
    }

    public void ChangeQuestionLevel(int id)
    {
        questionSO.questionsData.questions[editingQuestionId].levelOfKnowledge = questionSO.questionsData.categories[editingQuestionId].levelsOfKnowledge[id];
        SetQuestionsEditor(editingQuestionId);
    }
    #endregion
}
