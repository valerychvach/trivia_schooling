﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryExplorerIconsItem : MonoBehaviour
{

    public int id;
    public bool currentlySelected;
    public Image icon;
    public GameObject selectedGO;
    public GameEvent Event_ChangeIcon;

    public void Set(int id, bool selected, Sprite sprite)
    {
        this.id = id;
        currentlySelected = selected;
        selectedGO.SetActive(selected);
        icon.sprite = sprite;
    }

    public void ChangeIcon()
    {
        Event_ChangeIcon.Raise(id);
    }
}
