﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class CategoryExplorerManager : MonoBehaviour
{
    [InlineEditor]
    public QuestionsSO questionSO;
    [ReadOnly]
    public Sprite[] icons;

    [FoldoutGroup("Category explorer")]
    public Transform content_categoryExplorer;
    [FoldoutGroup("Category explorer")]
    public GameObject categoryExplorerItem;

    [FoldoutGroup("Category editor")]
    public InputField categoryEditorName;
    [FoldoutGroup("Category editor")]
    public Transform content_categoryIconEditor;
    [FoldoutGroup("Category editor")]
    public GameObject categoryIconItem;
    [FoldoutGroup("Category editor")]
    public Transform content_categoryLevelEditor;
    [FoldoutGroup("Category editor")]
    public GameObject categoryLevelItem;

    int editingCategoryId;
    void Awake()
    {
        icons = Resources.LoadAll<Sprite>("CategoryIcons");
    }

    void Start()
    {
        SetCategoryExplorer();
    }

    #region Category explorer
    [Button("Set category explorer")]
    public void SetCategoryExplorer()
    {
        if (content_categoryExplorer.childCount > 0)
            //Erase all children
            for (int i = content_categoryExplorer.childCount - 1; i >= 0; i--)
            {
                Destroy(content_categoryExplorer.GetChild(i).gameObject);
            }

        //Fill content with all saved categories
        for (int i = 0; i < questionSO.questionsData.categories.Count; i++)
        {
            CategoryExplorerItem c = ((GameObject)Instantiate(categoryExplorerItem, content_categoryExplorer)).GetComponent<CategoryExplorerItem>();
            c.SetItem(i, questionSO.questionsData.categories[i].name, icons[questionSO.questionsData.categories[i].spriteId]);
        }
        content_categoryExplorer.GetComponent<RectTransform>().sizeDelta = new Vector2(
            content_categoryExplorer.GetComponent<RectTransform>().sizeDelta.x,
            categoryExplorerItem.GetComponent<RectTransform>().sizeDelta.y * questionSO.questionsData.categories.Count);
    }

    public void RemoveCategory(int id)
    {
        questionSO.questionsData.categories.RemoveAt(id);
        SetCategoryExplorer();
    }
    #endregion

    #region Category editor
    public void SetCategoryToEdit()
    {
        //CATEGORY NAME
        categoryEditorName.text = questionSO.questionsData.categories[editingCategoryId].name;

        //CATEGORY ICONS
        if (content_categoryIconEditor.childCount > 0)
            //Erase all children
            for (int i = content_categoryIconEditor.childCount - 1; i >= 0; i--)
            {
                Destroy(content_categoryIconEditor.GetChild(i).gameObject);
            }

        //Fill content with all saved categories
        for (int i = 0; i < icons.Length; i++)
        {
            CategoryExplorerIconsItem c = ((GameObject)Instantiate(categoryIconItem, content_categoryIconEditor)).GetComponent<CategoryExplorerIconsItem>();
            c.Set(i, i == questionSO.questionsData.categories[editingCategoryId].spriteId, icons[i]);
        }
        content_categoryIconEditor.GetComponent<RectTransform>().sizeDelta = new Vector2(
            content_categoryIconEditor.GetComponent<RectTransform>().sizeDelta.x,
            categoryIconItem.GetComponent<RectTransform>().sizeDelta.y * (Mathf.FloorToInt(icons.Length / 3) + 1));

        //CATEGORY LEVELS
        if (content_categoryLevelEditor.childCount > 0)
            //Erase all children
            for (int i = content_categoryLevelEditor.childCount - 1; i >= 0; i--)
            {
                Destroy(content_categoryLevelEditor.GetChild(i).gameObject);
            }

        //Fill content with all saved categories
        for (int i = 0; i < questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge.Count; i++)
        {
            CategoryExplorerLevelsItem c = ((GameObject)Instantiate(categoryLevelItem, content_categoryLevelEditor)).GetComponent<CategoryExplorerLevelsItem>();
            c.Set(i, questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge[i], questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledgeXP[i]);
        }
        content_categoryLevelEditor.GetComponent<RectTransform>().sizeDelta = new Vector2(
            content_categoryLevelEditor.GetComponent<RectTransform>().sizeDelta.x,
            categoryLevelItem.GetComponent<RectTransform>().sizeDelta.y * questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge.Count);

    }
    public void SetCategoryToEdit(int categoryId)
    {
        editingCategoryId = categoryId;
        if (categoryId >= 0)
        {
            //CATEGORY NAME
            categoryEditorName.text = questionSO.questionsData.categories[categoryId].name;

            //CATEGORY ICONS
            if (content_categoryIconEditor.childCount > 0)
                //Erase all children
                for (int i = content_categoryIconEditor.childCount - 1; i >= 0; i--)
                {
                    Destroy(content_categoryIconEditor.GetChild(i).gameObject);
                }

            //Fill content with all saved categories
            for (int i = 0; i < icons.Length; i++)
            {
                CategoryExplorerIconsItem c = ((GameObject)Instantiate(categoryIconItem, content_categoryIconEditor)).GetComponent<CategoryExplorerIconsItem>();
                c.Set(i, i == questionSO.questionsData.categories[categoryId].spriteId, icons[i]);
            }
            content_categoryIconEditor.GetComponent<RectTransform>().sizeDelta = new Vector2(
                content_categoryIconEditor.GetComponent<RectTransform>().sizeDelta.x,
                categoryIconItem.GetComponent<RectTransform>().sizeDelta.y * (Mathf.FloorToInt(icons.Length / 3) + 1));

            //CATEGORY LEVELS
            if (content_categoryLevelEditor.childCount > 0)
                //Erase all children
                for (int i = content_categoryLevelEditor.childCount - 1; i >= 0; i--)
                {
                    Destroy(content_categoryLevelEditor.GetChild(i).gameObject);
                }

            //Fill content with all saved categories
            for (int i = 0; i < questionSO.questionsData.categories[categoryId].levelsOfKnowledge.Count; i++)
            {
                CategoryExplorerLevelsItem c = ((GameObject)Instantiate(categoryLevelItem, content_categoryLevelEditor)).GetComponent<CategoryExplorerLevelsItem>();
                c.Set(i, questionSO.questionsData.categories[categoryId].levelsOfKnowledge[i], questionSO.questionsData.categories[categoryId].levelsOfKnowledgeXP[i]);
            }
            content_categoryLevelEditor.GetComponent<RectTransform>().sizeDelta = new Vector2(
                content_categoryLevelEditor.GetComponent<RectTransform>().sizeDelta.x,
                categoryLevelItem.GetComponent<RectTransform>().sizeDelta.y * questionSO.questionsData.categories[categoryId].levelsOfKnowledge.Count);
        }
        else    //If a new category is being added
        {
            //Create an empty new category
            Category c = new Category("Category name", 0);
            c.levelsOfKnowledge = new List<string>();
            c.levelsOfKnowledgeXP = new List<int>();
            c.levelsOfKnowledge.Add("Some level");
            c.levelsOfKnowledgeXP.Add(100);
            questionSO.questionsData.categories.Add(c);
            //Recall the function with the new category Id
            SetCategoryToEdit(questionSO.questionsData.categories.Count - 1);
        }
    }

    public void RemoveCategoryLevel(int levelId)
    {
        if (questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge.Count <= 1)
            return;
        questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge.RemoveAt(levelId);
        questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledgeXP.RemoveAt(levelId);
        SetCategoryToEdit();
    }

    public void AddCategoryLevel()
    {
        questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge.Add("Some level");
        questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledgeXP.Add(100);
        SetCategoryToEdit();
    }

    public void ChangeCategoryName(string name)
    {
        questionSO.questionsData.categories[editingCategoryId].name = name.Replace("\"", "'");
    }

    public void ChangeCategoryIcon(int id)
    {
        questionSO.questionsData.categories[editingCategoryId].spriteId = id;
        SetCategoryToEdit();
    }

    public void ChangeCategoryLevelName(string data)
    {
        string[] d = data.Split(',');
        questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledge[int.Parse(d[0])] = d[1].Replace("\"", "'");
    }

    public void ChangeCategoryLevelXP(string data)
    {
        string[] d = data.Split(',');
        questionSO.questionsData.categories[editingCategoryId].levelsOfKnowledgeXP[int.Parse(d[0])] = int.Parse(d[1]);
    }
    #endregion
}
