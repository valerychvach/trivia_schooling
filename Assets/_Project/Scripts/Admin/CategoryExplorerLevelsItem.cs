﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryExplorerLevelsItem : MonoBehaviour
{
    public int id;
    public InputField name;
    public InputField xp;

    public GameEvent Event_RemoveLevel;
    public GameEvent Event_ChangeCategoryLevelName;
    public GameEvent Event_ChangeCategoryLevelXP;

    public void Set(int id, string name, int xp)
    {
        this.id = id;
        this.name.text = name;
        this.xp.text = xp + "";
    }

    public void Remove()
    {
        Event_RemoveLevel.Raise(id);
    }

    public void ChangeCategoryLevelName(string name)
    {
        Event_ChangeCategoryLevelName.Raise(id + "," + name);
    }

    public void ChangeCategoryLevelXP(string value)
    {
        Event_ChangeCategoryLevelXP.Raise(id + "," + value);
    }
}
