﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuestionsExplorerCategoryItem : MonoBehaviour
{

    public int id;
    public TextMeshProUGUI categoryName;
    public Image icon;

    public GameEvent Event_ViewCategoryQuestions;
    public void SetItem(int id, string name, Sprite sprite)
    {
        this.id = id;
        categoryName.text = name;
        icon.sprite = sprite;
    }

    public void ViewCategoryQuestions()
    {
        Event_ViewCategoryQuestions.Raise(id);
    }
}
