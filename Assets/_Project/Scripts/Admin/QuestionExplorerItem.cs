﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestionExplorerItem : MonoBehaviour
{
    public int id;
    public TextMeshProUGUI question;
    public TextMeshProUGUI level;
    public GameEvent Event_EditQuestion;
    public GameEvent Event_RemoveQuestion;

    public void Set(int id, string question, string level)
    {
        this.id = id;
        this.question.text = question;
        this.level.text = level;
    }

    public void EditQuestion()
    {
        Event_EditQuestion.Raise(id);
    }

    public void RemoveQuestion()
    {
        Event_RemoveQuestion.Raise(id);
    }
}
