﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CategoryExplorerItem : MonoBehaviour
{
    public int id;
    public TextMeshProUGUI categoryName;
    public Image icon;

    public GameEvent Event_OnRemoveCategory;
    public GameEvent Event_OnEditCategory;

    public void SetItem(int id, string name, Sprite sprite)
    {
        this.id = id;
        categoryName.text = name;
        icon.sprite = sprite;
    }

    public void EditCategory()
    {
        Event_OnEditCategory.Raise(id);
    }

    public void RemoveCategory()
    {
        Event_OnRemoveCategory.Raise(id);
    }
}
