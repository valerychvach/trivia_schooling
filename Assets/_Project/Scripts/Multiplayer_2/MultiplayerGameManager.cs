﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameSparks.Api.Responses;
using UnityEngine.Events;
using GameSparks.Core;
using TMPro;
using UnityEngine.UI;
using GameSparks.RT;

public class MultiplayerGameManager : MonoBehaviour
{
    // Before game objects
    [SerializeField] private TextMeshProUGUI readyText;
    [SerializeField] private Transform readyBtn;
    // In game objects
    [SerializeField] private Transform quizActive;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private TextMeshProUGUI questionsText;
    [SerializeField] private TextMeshProUGUI resultText;
    [SerializeField] private Transform gameResultsActive;
    [SerializeField] private int numQuestions = 10;
    [SerializeField] private int timeOnQuestion = 10;
    // Buttons and sprites
    [SerializeField] private List<Image> answerBtn;
    [SerializeField] private List<Sprite> colorSprites;
    [SerializeField] private TextMeshProUGUI textQuestion;

    private List<QuestionTest> questions = new List<QuestionTest>();
    private int numRightAnswers = 0;
    private int timeToAnswers = 0;
    private int currentQuestion = 0;
    private int currentTimer = 0;
    private IEnumerator btnCoroutine;

    private int numPlayersReady = 0;
    private int finishedPlayers = 0;
    private List<PlayerResult> playerResults = new List<PlayerResult>();
    private int numCurrentPlayer;

    // Instance for GameSparkManager
    private static MultiplayerGameManager instance;
    public static MultiplayerGameManager Instance()
    {
        return instance;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        // for delete
        questions.Add(new QuestionTest("Question1", 1));
        questions.Add(new QuestionTest("Question2", 2));
        questions.Add(new QuestionTest("Question3", 3));
        questions.Add(new QuestionTest("Question4", 0));
        questions.Add(new QuestionTest("Question5", 3));
        questions.Add(new QuestionTest("Question6", 2));
        questions.Add(new QuestionTest("Question7", 1));
        questions.Add(new QuestionTest("Question8", 0));
        questions.Add(new QuestionTest("Question9", 1));
        questions.Add(new QuestionTest("Question10", 3));
    }

    #region Start playing

    public void ReadyOponents(RTPacket _packet)
    {
        string senderName = GameSparksManager.Instance().GetSessionInfo().GetPlayerList().Find(x => x.peerId == _packet.Sender).displayName;
        readyText.text = senderName + " ready!";
        numPlayersReady++;

        if (numPlayersReady == GameSparksManager.Instance().GetSessionInfo().GetPlayerList().Count)
            StartCoroutine("Starting");
    }

    public void ReadyToPlay()
    {
        readyBtn.gameObject.SetActive(false);

        numCurrentPlayer = GameSparksManager.Instance().GetSessionInfo().GetPlayerList().FindIndex(x => x.peerId == GameSparksManager.Instance().GetRTSession().PeerId);
        numPlayersReady++;

        // send oponents - ready to play
        using (RTData data = RTData.Get())
        {  // we put a using statement here so that we can dispose of the RTData objects once the packet is sent
            data.SetInt(2, 0);
            GameSparksManager.Instance().GetRTSession().SendData(2, GameSparksRT.DeliveryIntent.UNRELIABLE_SEQUENCED, data);// send the data
        }

        if (numPlayersReady == GameSparksManager.Instance().GetSessionInfo().GetPlayerList().Count)
            StartCoroutine("Starting");
    }

    private IEnumerator Starting()
    {
        for (int i = 3; i > 0; i--)
        {
            readyText.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        readyText.text = "Go!";
        yield return new WaitForSeconds(1f);
        readyText.gameObject.SetActive(false);

        StopCoroutine("Starting");
        PrepareToStartQuiz();
    }

    private void PrepareToStartQuiz()
    {
        // Timer and Num questions
        quizActive.gameObject.SetActive(true);
        timerText.text = (timeOnQuestion * numQuestions).ToString();
        questionsText.text = "1/" + numQuestions.ToString();

        SetNewQuestion();
        StartCoroutine("Timer");
    }

    private IEnumerator Timer()
    {
        for (currentTimer = numQuestions * timeOnQuestion; currentTimer >= 0; currentTimer--)
        {
            timerText.text = currentTimer.ToString();
            yield return new WaitForSeconds(1f);
        }

        StopCoroutine("Timer");
    }
    #endregion

    public void ClickButton(int answer)
    {
        btnCoroutine = ButtonsSetTrueFalseSptite(answer);
        StartCoroutine(btnCoroutine);
    }

    private IEnumerator ButtonsSetTrueFalseSptite(int answer)
    {
        if (CheckAnswer(answer))
        {
            numRightAnswers++; // set +1 to right answers
            answerBtn[answer].sprite = colorSprites[1]; // color true
        }
        else
        {
            answerBtn[answer].sprite = colorSprites[2]; // color false
            answerBtn[questions[currentQuestion].Answer].sprite = colorSprites[1]; // color true
        }

        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < answerBtn.Count; i++)
        {
            answerBtn[i].sprite = colorSprites[0]; // normal color for all answer buttons
        }
        currentQuestion++; // set num current question
        SetNewQuestion(); // displayed next question
        StopCoroutine("ButtonsSetTrueFalseSptite");
    }

    private bool CheckAnswer(int answer)
    {
        return questions[currentQuestion].Answer == answer ? true : false;
    }

    private void SetNewQuestion()
    {
        if (currentQuestion < numQuestions)
        {
            textQuestion.text = questions[currentQuestion].Question;
            questionsText.text = currentQuestion + 1 + "/" + numQuestions.ToString();
            // add current answers on buttons

        }

        if (currentQuestion == numQuestions)
        {
            quizActive.gameObject.SetActive(false);
            finishedPlayers++;

            // send oponents results and time on answers
            using (RTData data = RTData.Get())
            {  // we put a using statement here so that we can dispose of the RTData objects once the packet is sent
                Vector2 dataToSend = new Vector2(
                    numRightAnswers, // number of right answers
                    (100 - currentTimer)); // time on answers
                data.SetVector3(3, dataToSend);
                GameSparksManager.Instance().GetRTSession().SendData(3, GameSparksRT.DeliveryIntent.UNRELIABLE_SEQUENCED, data);// send the data
            }

            playerResults.Add(new PlayerResult((int)GameSparksManager.Instance().GetRTSession().PeerId, numRightAnswers, (100 - currentTimer)));

            resultText.text = "Waiting for opponents";

            if (finishedPlayers == GameSparksManager.Instance().GetSessionInfo().GetPlayerList().Count)
                GameResult();
        }
    }

    public void UpdateDataGameResult(RTPacket _packet)
    {
        switch (_packet.OpCode)
        {
            case 3:
                finishedPlayers++;
                playerResults.Add(new PlayerResult(_packet.Sender, _packet.Data.GetVector2(3).Value.x, _packet.Data.GetVector2(3).Value.y));
                resultText.text = "Waiting for opponents";
                if (finishedPlayers == GameSparksManager.Instance().GetSessionInfo().GetPlayerList().Count)
                    GameResult();
                break;
        }
    }

    private void GameResult()
    {
        Debug.Log("Game result");
        // Game result
        List<float> listAnswerResult = new List<float>();
        List<float> listTimeResult = new List<float>();
        foreach (var res in playerResults)
            listAnswerResult.Add(res.RightAnswers);

        listAnswerResult.Sort(new ComparerFloat());

        if (listAnswerResult[listAnswerResult.Count - 2] == listAnswerResult[listAnswerResult.Count - 1])
        {
            foreach (var res in playerResults)
                listTimeResult.Add(res.AnswerTime);

            listTimeResult.Sort(new ComparerFloat());
            PlayerResult winner = playerResults.Find(x => x.AnswerTime == listTimeResult[0]);
            int winnerIndex = GameSparksManager.Instance().GetSessionInfo().GetPlayerList().FindIndex(x => x.peerId == winner.PeerId);

            resultText.text = GameSparksManager.Instance().GetSessionInfo().GetPlayerList()[winnerIndex].displayName + " winner!";
        }
        else
        {
            PlayerResult winner = playerResults.Find(x => x.RightAnswers == listAnswerResult[listAnswerResult.Count - 1]);
            int winnerIndex = GameSparksManager.Instance().GetSessionInfo().GetPlayerList().FindIndex(x => x.peerId == winner.PeerId);
            resultText.text = GameSparksManager.Instance().GetSessionInfo().GetPlayerList()[winnerIndex].displayName + " winner!";
        }

        gameResultsActive.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(2);
    }

    #region Button actions (answer)
    public void ClickButton_1()
    {
        ClickButton(0);
    }
    public void ClickButton_2()
    {
        ClickButton(1);
    }
    public void ClickButton_3()
    {
        ClickButton(2);
    }
    public void ClickButton_4()
    {
        ClickButton(3);
    }
    #endregion

    #region Additional classes
    public class QuestionTest
    {
        public string Question;
        public int Answer;
        //public string quest1, quest2, quest3, quest4;

        public QuestionTest(string _quest, int _ans)
        {
            Question = _quest;
            Answer = _ans;
        }
    }

    public class PlayerResult
    {
        public int PeerId;
        public float RightAnswers, AnswerTime;

        public PlayerResult(int peerId, float rightAnswers, float answerTime)
        {
            PeerId = peerId;
            RightAnswers = rightAnswers;
            AnswerTime = answerTime;
        }
    }
    #endregion
}
